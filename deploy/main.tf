terraform {
  backend "s3" {
    bucket         = "tfstate-4"
    key            = "recipe-app.tfstate"
    region         = "eu-west-1"
    encrypt        = true
    dynamodb_table = "tfstate-4-lock"
  }
}

provider "aws" {
  region  = "eu-west-1"
  version = "~> 2.50.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

data "aws_region" "current" {}